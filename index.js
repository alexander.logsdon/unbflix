// index.js

const https = require('https');
const fs = require('fs');

const host = 'netflix.com';

function updateHTML(page) {
  const html = `
    <!DOCTYPE html>
    <html>
      <head>
        <meta http-equiv="Refresh" content="0; url='${page}'" /> 
      </head>
    </html>
  `;
  
  fs.writeFileSync('index.html', html); 
}

https.createServer((req, res) => {
  let page = '/';
  
  if (req.url === '/') {
    page = `https://${host}`;
  }
  if (req.url === '/title') {
    page = `https://${host}/title`;
  }
  if (req.url === '/gb/login') {
    page = `https://${host}/gb/login`;
  }

  updateHTML(page);
  
  res.end();

}).listen(8080);
